const Order = require('../models/Order')
const Product = require('../models/Product')

module.exports.createOrder = async (data) => {


		// let product_price = Product.findOne({price: {$gt: 0}}, (error, result) => {
		// 	// if (error) {
		// 	// 	console.log(error)
		// 	// 	return error
		// 	// } 
		// 	// product_price = result[0].price
		// 	//checking if we are getting the price correctly
		// 	// console.log(result.price)
		// 	// product_price = result.price
		// 	console.log(result.price)
		// 	return result.price
			
		// })

		let product_price = await Product.findOne({price: {$gt: 0}}, 'price').then((error, result) => {
			if (error) {
				console.log(error)
				return error
			} else {
				return result
			}
		})

		//check if we are getting the correct data
		// console.log(product_price.price)
		// console.log(typeof product_price.price)
		

	
	// let price = Number(getPrice())
	// console.log(typeof find_price)
	// console.log(find_price[0].price)
	// console.log(find_price)

	// let price = Number(getPrice)
	// console.log(price)

	// console.log(getPrice)
	// console.log(typeof getPrice)

	
	// Product.findOne({price: {$gt: 0}}).then((error, result) => {
	// 	if (error) {
	// 		console.log(error)
	// 		return error
	// 	}
	// 		// checking if we are getting the price correctly
	// 		// console.log(result.price)
	// 		// console.log(result)
	// 		// return result
	// })


	// console.log(typeof find_price)
	// console.log(find_price.price)
	// console.log(product_price)
	// console.log(product_price[0].price) error - cannot read properties of undefined ('reading price')

		if (data.isAdmin) {

			return Promise.resolve({
				message: "ADMIN is not allowed to check out."
			})
		} 

		else {
			let new_order = new Order({
			userId: data.userId,
			products: [{
				productId: data.productId, 
				quantity: data.quantity
			}],
			totalAmount: product_price.price * data.quantity

		})

			return new_order.save().then((result, error) => {
			if (error) {
				return false
			}

				return Promise.resolve({
			message: "Order successfully created."
		})
		})
		}

	}


module.exports.getOrders = (data) => {

	if (data.isAdmin) {
		return Order.find({}).then((result, error) => {
		if (error) {
			return false
		}

		return result
	})
	}

	return Promise.resolve({
		message: "Only ADMIN can retrieve orders."
	})
}