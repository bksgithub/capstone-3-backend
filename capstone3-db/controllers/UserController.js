const User = require('../models/User')
const Product = require('../models/Product')
const bcrypt = require('bcrypt')
const auth = require('../auth')

module.exports.checkIfEmailExists = (data) => {
	return User.find({email: data.email}).then((result) => {
		if(result.length > 0){
			return true
		}

		return false
	})
}

module.exports.register = (data) => {
	let encrypted_password = bcrypt.hashSync(data.password, 10)

	let new_user = new User({
		firstName: data.firstName,
		lastName: data.lastName,
		email: data.email,
		mobileNo: data.mobileNo,
		password: encrypted_password
	})

	return new_user.save().then((created_user, error) => {
		if(error){
			return false 
		}

		return {
			message: 'User successfully registered!'
		}
	})
}

module.exports.login = (data) => {
	return User.findOne({email: data.email}).then((result) => {
		if(result == null){
			return {
				message: "User doesn't exist!"
			}
		}

		const is_password_correct = bcrypt.compareSync(data.password, result.password)

		if(is_password_correct) {
			return {
				accessToken: auth.createAccessToken(result)
			}
		}

		return {
			message: 'Password is incorrect!'
		}
	})
}

// module.exports.getUserDetails = (user_id) => {
// 	return User.findById(user_id, {password: 0}).then((result) => {
// 		return result
// 	})
// }

// PASTE TO CONTROLLER

// Get single user based on ID from token
module.exports.getProfile = (data) => {

	return User.findById(data.userId).then(result => {

		// Makes the password not be included in the result
		result.password = "";

		// Returns the user information with the password as an empty string
		return result;

	});
};

module.exports.buy = async (data) => {
	// Check if user is done adding the account to its accountsBought array
	let is_user_updated = await User.findById(data.userId).then((user) => {
		user.accountsBought.push({
			productId: data.productId
		})

		return user.save().then((updated_user, error) => {
			if(error){
				return false
			}

			return true
		}) 
	})

	// Check if product is done adding the user to its owner array
	let is_product_updated = await Product.findById(data.productId).then((product) => {
		product.owner.push({
			userId: data.userId
		})

		return product.save().then((updated_product, error) => {
			if(error){
				return false
			}

			return true
		}) 
	})

	// Check if both the user and product have been updated successfully, and return a success message if so
	if(is_user_updated && is_product_updated){
		return {
			message: 'You bought the account successfully!'
		}
	}

	// If the enrollment failed, return 'Something went wrong.'
	return {
		message: 'Something went wrong.'
	}
}