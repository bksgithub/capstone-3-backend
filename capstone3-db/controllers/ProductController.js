const Product = require('../models/Product')

module.exports.addProduct = (data) => {
	if(data.isAdmin){
		let new_product = new Product({
			name: data.product.name,
			description: data.product.description,
			price: data.product.price
		})
		
		return new_product.save().then((new_prouct, error) => {
			if(error){
				return false
			}

			return {
				message: 'New product successfully created!'
			}
		})
	}

	let message = Promise.resolve({
		message: 'User must be ADMIN to create product.'
	})

	return message.then((value) => {
		return value
	})
}

module.exports.getAllProducts = (data) => {

	if (data.isAdmin) {
		return Product.find({}).then((result) => {
		return result
	})
	} else {
		return Promise.resolve(
			{
			message: "Only ADMINS can get all products."
		})
	}
}

module.exports.getAllActive = () => {
	return Product.find({isActive: true}).then((result) => {
		return result
	})
}

module.exports.getCourse = (course_id) => {
	return Course.findById(course_id).then((result) => {
		return result 
	})
}

module.exports.updateProduct = (data) => {

	if (data.isAdmin) {
		return Product.findByIdAndUpdate(data.product_id, {
		name: data.product_update.name,
		description: data.product_update.description, 
		price: data.product_update.price,
		isActive: data.product_update.isActive 
	}).then((updated_product, error) => {
		if(error){
			return false
		}

		return {
			message: 'Product has been updated successfully!'
		}
	})
} else {
	return Promise.resolve(
			{
				message: `User must be ADMIN to update products.`
			}
		)
}
}

module.exports.archiveProduct = (data) => {
	if (data.isAdmin) {
		return Product.findByIdAndUpdate(data.product_id, {
		isActive: false
	}).then((archived_product, error) => {
		if(error){
			return false
		}

		return {
			message: 'Product has been deactivated successfully!'
		}
	})
} else {
	return Promise.resolve(
		{
		message: `User must be ADMIN to deactivate products.`
	})
}
}

module.exports.unarchiveProduct = (data) => {

	if (data.isAdmin) {
		return Product.findByIdAndUpdate(data.product_id, {
		isActive: true
	}).then((unarchived_product, error) => {
		if(error){
			return false
		}

		return {
			message: 'Product has been reactivated successfully!'
		}
	})
	} else {
		return Promise.resolve(
				{
					message: `User must be ADMIN to reactivate products`
				}
			)
	}
}

module.exports.getProduct = (product_id) => {
	return Product.findById(product_id).then((error, result) => {
		if (error) {
			console.log(error)
			return error
		} else {
			console.log(result)
			return result
		}
	})
}