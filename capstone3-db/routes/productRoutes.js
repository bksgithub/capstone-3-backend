const express = require('express')
const router = express.Router()
const ProductController = require('../controllers/ProductController')
const auth = require('../auth')

// Create single product
router.post('/create', auth.verify, (request, response) => {
	const data = {
		product: request.body,
		isAdmin: auth.decode(request.headers.authorization).isAdmin
	}

	ProductController.addProduct(data).then((result) => {
		response.send(result)
	})
})

// Get all products
router.get('/', auth.verify, (request, response) => {

	const data = {
		isAdmin: auth.decode(request.headers.authorization).isAdmin
	}

	ProductController.getAllProducts(data).then((result) => {
		response.send(result)
	})
})

// Get all ACTIVE products
router.get('/active', (request, response) => {
	ProductController.getAllActive().then((result) => {
		response.send(result)
	})
})

// Get single product
router.get('/:productId', (request, response) => {
	ProductController.getProduct(request.params.productId).then((result) => {
		response.send(result)
	})
})

// Update single product
router.patch('/:productId/update', auth.verify, (request, response) => {

	const data = {
		product_id: request.params.productId,
		product_update: request.body,
		isAdmin: auth.decode(request.headers.authorization).isAdmin,
		isActive: request.body
	}

	ProductController.updateProduct(data).then((result) => {
		response.send(result)
	})
})

// Archive single product
router.patch('/:productId/archive', auth.verify, (request, response) => {

	const data = {
		isAdmin: auth.decode(request.headers.authorization).isAdmin,
		product_id: request.params.productId
	}

	ProductController.archiveProduct(data).then((result) => {
		response.send(result)
	})
})

//Unarchive single product
router.patch('/:productId/unarchive', auth.verify, (request, response) => {

	const data = {
		isAdmin: auth.decode(request.headers.authorization).isAdmin,
		product_id: request.params.productId
	}

	ProductController.unarchiveProduct(data).then((result) => {
		response.send(result)
	})
})


module.exports = router 