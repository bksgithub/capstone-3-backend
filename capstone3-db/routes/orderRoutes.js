const express = require('express')
const router = express.Router()
const OrderController = require('../controllers/OrderController')
const auth = require('../auth')

router.post("/create-order", auth.verify, (request, response) => {
	const data = {
		isAdmin: auth.decode(request.headers.authorization).isAdmin,
		userId: request.body.userId,
		productId: request.body.productId, 
		quantity: request.body.quantity,
		// totalAmount: request.body.totalAmount
	}

	OrderController.createOrder(data).then((result) => {
		response.send(result)
	})
})

router.get("/retrieve-orders", auth.verify, (request, response) => {

	const data = {
		isAdmin: auth.decode(request.headers.auth).isAdmin
	}

	OrderController.getOrders(data).then((result) => {
		response.send(result)
	})
})

module.exports = router 