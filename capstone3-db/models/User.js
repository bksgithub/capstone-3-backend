const mongoose = require('mongoose')

const user_schema = new mongoose.Schema({
	firstName: {
		type: String,
		required: [true, "First Name is required."]
	},
	lastName: {
		type: String,
		required: [true, "Last Name is required."]
	},
	email: {
		type: String,
		required: [true, "Email is required."]
	},
	password: {
		type: String,
		required: [true, "Password is required."]
	},
	isAdmin: {
		type: Boolean,
		default: false
	},
	mobileNo: {
		type: String,
		required: [true, 'Mobile Number is required.']
	}, accountsBought: [
		{
			productId: {
				type: String,
				required: [true, 'product ID is required.']
			},
			boughtOn: {
				type: Date,
				default: new Date()
			},
			status: {
				type: String,
				default: "Account bought"
			}
		}
	]

})

module.exports = mongoose.model('User', user_schema)