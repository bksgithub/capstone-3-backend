const mongoose = require("mongoose")

const order_schema = new mongoose.Schema({

userId: {
	type: String,
	required: [true, "User ID is required"]
},
products: [
	{
		productId: {
		type: String,
		required: [true, "Product ID is required."]
		},
		quantity: {
			type: Number,
			required: [true, "Product quantity is required"]
		}
	}
],
totalAmount: {
	type: Number
},
purchasedOn: {
	type: Date,
	default: new Date()
}
})

module.exports = mongoose.model("Orders", order_schema)