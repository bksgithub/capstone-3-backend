const mongoose = require('mongoose')

const product_schema = new mongoose.Schema({
	name: {
		type: String,
		required: [true, 'Product name is required.']
	},
	description: {
		type: String,
		required: [true, 'Description is required.']
	},
	price: {
		type: Number,
		required: [true, 'Price is required.']
	},
	isActive: {
		type: Boolean,
		default: true
	},
	createdOn: {
		type: Date,
		default: new Date()
	}, owner: [
		{
			userId: {
				type: String,
				required: [true, 'UserID is Required.']
			},
			ownedOn: {
				type: Date,
				default: new Date()
			}
		}
	]

})

module.exports = mongoose.model('Product', product_schema)